@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="page-header">
            <h2>Listagem de usuários <small><a href="{{route('admin.users.create')}}"><span class="glyphicon glyphicon-plus"></span> Novo usuário</a></small></h2>
        </div>
        @if(Session::has('success'))
            <div class="alert alert-success fade in">
                {{ Session::get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td width="7%" class="text-center" rowspan>
                        <div class="btn-group btn-group-xs">
                            <a href="{{route('admin.users.edit', $user->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
                            <a href="{{ route('admin.users.show', $user->id) }}" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $users->render() !!}
    </div>
@endsection