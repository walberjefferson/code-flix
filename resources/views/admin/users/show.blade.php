@extends('layouts.admin')

@section('content')
    <div class="container">

        <div class="page-header">
            <h2>Dados do usuário: <strong>{{ $user->name }}</strong></h2>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">Informações do Usuário</h4>
                    </div>

                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th class="text-center" scope="row">#</th>
                            <td>{{ $user->id }}</td>
                        </tr>
                        <tr>
                            <th width="5%" scope="row">Nome:</th>
                            <td>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <th scope="row">E-mail:</th>
                            <td>{{ $user->email }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
                <div class="btn-group">
                    <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> Editar</a>
                    <a href="{{ route('admin.users.destroy', $user->id) }}" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('form-delete').submit()"><span class="glyphicon glyphicon-trash"></span> Excluir</a>
                    @php $formDelete = FormBuilder::plain(['route' => ['admin.users.destroy', $user->id], 'id' => 'form-delete', 'method' => 'DELETE', 'style' => 'display:none']) @endphp
                    {!! form($formDelete) !!}
                </div>
            </div>
        </div>
    </div>
@endsection