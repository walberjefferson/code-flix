@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="page-header">
            <h2>Categorias
                <small>| <a href="{{ route('admin.categories.create') }}">Nova Categoria</a></small>
            </h2>
        </div>

        @if(Session::has('success'))
            <div class="alert alert-success fade in">
                {{ Session::get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <table class="table table-striped">
            <thead>
            <tr>
                <th width="5%" rowspan>#</th>
                <th>Nome</th>
                <th width="8%">Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>
                        <div class="btn-group btn-group-xs">
                            <a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
                            <a href="{{ route('admin.categories.show', $category->id) }}" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $categories->render() !!}
    </div>
@endsection