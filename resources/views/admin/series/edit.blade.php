@extends('layouts.admin')

@section('content')
    <div class="container">

        <div class="page-header">
            <h2>Alterar serie</h2>
        </div>

        @if(Session::has('error'))
            <div class="alert alert-danger fade in">
                {{ Session::get('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                {!! form($form->add('salvar', 'submit', [
                    'attr' => ['class' => 'btn btn-primary btn-block'],
                    'label' => '<span class="glyphicon glyphicon-floppy-disk"></span> Salvar'
                ])) !!}
            </div>
        </div>
    </div>
@endsection