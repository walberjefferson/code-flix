@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="page-header">
            <h2>Series
                <small>| <a href="{{ route('admin.series.create') }}">Nova Serie</a></small>
            </h2>
        </div>

        @if(Session::has('success'))
            <div class="alert alert-success fade in">
                {{ Session::get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <table class="table table-striped">
            <thead>
            <tr>
                <th width="5%" rowspan>#</th>
                <th>Titulo</th>
                <th>Descrição</th>
                <th width="8%">Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($series as $serie)
                <tr>
                    <td>{{ $serie->id }}</td>
                    <td>{{ $serie->title }}</td>
                    <td>{{ $serie->description }}</td>
                    <td>
                        <div class="btn-group btn-group-xs">
                            <a href="{{ route('admin.series.edit', $serie->id) }}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
                            <a href="{{ route('admin.series.show', $serie->id) }}" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $series->render() !!}
    </div>
@endsection