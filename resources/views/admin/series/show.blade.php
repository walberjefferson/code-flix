@extends('layouts.admin')

@section('content')
    <div class="container">

        <div class="page-header">
            <h2>Dados da serie: <strong>{{ $series->name }}</strong></h2>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">Informações da categoria</h4>
                    </div>

                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th class="text-center" scope="row">#</th>
                            <td>{{ $series->id }}</td>
                        </tr>
                        <tr>
                            <th width="5%" scope="row">Nome:</th>
                            <td>{{ $series->title }}</td>
                        </tr>
                        <tr>
                            <th width="5%" scope="row">Descrição:</th>
                            <td>{{ $series->description }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
                <div class="btn-group">
                    <a href="{{ route('admin.categories.edit', $series->id) }}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> Editar</a>
                    <a href="{{ route('admin.categories.destroy', $series->id) }}" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('form-delete').submit()"><span class="glyphicon glyphicon-trash"></span> Excluir</a>
                    @php $formDelete = FormBuilder::plain(['route' => ['admin.categories.destroy', $series->id], 'id' => 'form-delete', 'method' => 'DELETE', 'style' => 'display:none']) @endphp
                    {!! form($formDelete) !!}
                </div>
            </div>
        </div>
    </div>
@endsection