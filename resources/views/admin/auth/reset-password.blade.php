@extends('layouts.admin')

@section('content')
    <div class="container">

        <div class="page-header">
            <h2>Alteração de senha do usuário: {{ auth()->user()->name }}</h2>
        </div>

        {!! form($form->add('salvar', 'submit', [
            'attr' => ['class' => 'btn btn-primary btn-block'],
            'label' => '<span class="glyphicon glyphicon-floppy-disk"></span> Salvar'
        ])) !!}
    </div>
@endsection
