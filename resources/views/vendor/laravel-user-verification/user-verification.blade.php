@extends('layouts.admin')

<!-- Main Content -->
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Verificação falhou</h3>
                    </div>
                    <div class="panel-body">
                        <p><strong>Sua conta não pôde ser verificada.</strong></p>
                    </div>

                    <div class="panel-footer">
                        <a href="{{url('/')}}" class="btn btn-primary">Voltar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
