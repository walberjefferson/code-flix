<?php

namespace CodeFlix\Http\Controllers\Admin;

use CodeFlix\Forms\ResetPasswordForm;
use CodeFlix\Forms\UserForm;
use CodeFlix\Models\User;
use CodeFlix\Http\Controllers\Controller;
use CodeFlix\Repositories\UserRepository;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\Form;
use FormBuilder;

class UsersController extends Controller
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * UsersController constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->repository->paginate();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = \FormBuilder::create(UserForm::class, [
            'method' => "POST",
            'url' => route('admin.users.store')
        ]);
        return view('admin.users.create', compact('form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        /** @var Form $form */
        $form = FormBuilder::create(UserForm::class);
        $data = $form->getFieldValues();

        if (!$form->isValid()) {
            return redirect()->route('admin.users.create')->withErrors($form->getErrors())->withInput();
        }

        $this->repository->create($data);
        return redirect()->route('admin.users.index')->with('success', "Usuário cadastrado com sucesso.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \CodeFlix\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \CodeFlix\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $form = \FormBuilder::create(UserForm::class, [
            'method' => "PUT",
            'url' => route('admin.users.update', ['user' => $user->id]),
            'model' => $user
        ]);

        return view('admin.users.edit', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \CodeFlix\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        /** @var Form $form */
        $form = FormBuilder::create(UserForm::class, [
            'data' => ['id' => $id]
        ]);
        if (!$form->isValid()) {
            return redirect()->route('admin.users.create')->withErrors($form->getErrors())->withInput();
        }

        $data = array_except($form->getFieldValues(), ['role', 'password']);
        $this->repository->update($data, $id);

        return redirect()->route('admin.users.index')->with('success', "Usuário alterado com sucesso.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \CodeFlix\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('admin.users.index')->with('success', "Usuário excluido com sucesso.");
    }

}
