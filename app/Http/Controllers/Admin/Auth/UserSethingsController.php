<?php

namespace CodeFlix\Http\Controllers\Admin\Auth;

use CodeFlix\Forms\ResetPasswordForm;
use CodeFlix\Repositories\UserRepository;
use Illuminate\Http\Request;
use CodeFlix\Http\Controllers\Controller;
use FormBuilder;

class UserSethingsController extends Controller
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * UserSethingsController constructor.
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function resetPassword()
    {
        $form = FormBuilder::create(ResetPasswordForm::class, [
            'method' => "PUT",
            'url' => route('admin.reset-password-put'),
        ]);
        return view('admin.auth.reset-password', compact('form'));
    }

    public function resetPasswordUpdate(Request $request)
    {
        /** @var Form $form */
        /*$form = FormBuilder::create(ResetPasswordForm::class);
        $data = $form->getFieldValues();*/

        $this->validate($request, [
            'password' => 'required|string|min:6|confirmed'
        ]);

        $data = $request->only('password');
        $this->repository->update($data, auth()->user()->id);
        return redirect()->route('admin.users.index')->with('success', "Senha alterada com sucesso.");

    }
}
