<?php

namespace CodeFlix\Http\Controllers\Admin;

use CodeFlix\Forms\SerieForm;
use CodeFlix\Models\Serie;
use CodeFlix\Repositories\SerieRepository;
use CodeFlix\Http\Controllers\Controller;
use FormBuilder;

class SeriesController extends Controller
{
    /**
     * @var SerieRepository
     */
    private $repository;

    /**
     * SeriesController constructor.
     */
    public function __construct(SerieRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $series = $this->repository->paginate();
        return view('admin.series.index', compact('series'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = FormBuilder::create(SerieForm::class, [
            'url' => route('admin.series.store'),
            'method' => 'POST'
        ]);
        return view('admin.series.create', compact('form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        /** @var Form $form */
        $form = FormBuilder::create(SerieForm::class);
        $data = $form->getFieldValues();

        if (!$form->isValid()) {
            return redirect()->route('admin.series.create')->withErrors($form->getErrors())->withInput();
        }

        $this->repository->create($data);
        return redirect()->route('admin.series.index')->with('success', "Serie cadastrada com sucesso.");
    }

    /**
     * Display the specified resource.
     *
     * @param  Serie  $series
     * @return \Illuminate\Http\Response
     */
    public function show(Serie $series)
    {
        return view('admin.series.show', compact('series'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Serie $series
     * @return \Illuminate\Http\Response
     */
    public function edit(Serie $series)
    {
        $form = FormBuilder::create(SerieForm::class, [
            'method' => "PUT",
            'url' => route('admin.series.update', $series->id),
            'model' => $series
        ]);

        return view('admin.series.edit', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        /** @var Form $form */
        $form = FormBuilder::create(SerieForm::class);
        if (!$form->isValid()) {
            return redirect()->route('admin.series.edit', $id)->withErrors($form->getErrors())->withInput();
        }
        $data = $form->getFieldValues();
        $this->repository->update($data, $id);

        return redirect()->route('admin.series.index')->with('success', "Serie alterada com sucesso.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('admin.series.index')->with('success', "Serie excluida com sucesso.");
    }
}
