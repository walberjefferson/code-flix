<?php

namespace CodeFlix\Forms;

use Kris\LaravelFormBuilder\Form;

class ResetPasswordForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('password', 'password', [
                'rules' => 'required|string|min:6|max:16|confirmed',
//                'label' => 'Senha'
            ])
            ->add('password_confirmation', 'password', [
//                'label' => 'Repita a senha'
            ]);
    }
}
