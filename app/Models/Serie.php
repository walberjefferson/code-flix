<?php

namespace CodeFlix\Models;

use Illuminate\Database\Eloquent\Model;

class Serie extends Model
{
    protected $table = 'series';

    protected $fillable = [
        'title', 'description'
    ];

}
